import React from "react";
import { useNavigate } from "react-router-dom";

const Preference = () => {
  const navigate = useNavigate()
  return (
    <>
      <h2>Preference</h2>
      <button onClick={() => navigate("/")}> To Preference</button>
    </>
  );
};

export default Preference;

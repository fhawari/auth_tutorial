import { useContext } from "react"
import { Navigate, Outlet } from "react-router-dom"
import { AuthContext } from "./AuthProvider"

// const PrivateRoutes = ({token}) => {
const PrivateRoutes = () => {
  const { token } = useContext(AuthContext)
  return (
    token ? <Outlet /> : <Navigate to="/login" />
  )
}

export default PrivateRoutes
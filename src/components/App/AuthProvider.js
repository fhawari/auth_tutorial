import { createContext } from "react";
import useToken from "./useToken";


export const AuthContext = createContext({});

export const AuthProvider = ({children})=>{
    const {token, setToken, removeToken } = useToken();
    return(
        <AuthContext.Provider value={{token,setToken,removeToken}}>
            {children}
        </AuthContext.Provider>
    )
}
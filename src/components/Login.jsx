import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";
// import PropTypes from "prop-types";
import { useContext } from "react";
import { AuthContext } from "./App/AuthProvider";
// import { useNavigate } from "react-router-dom";

async function loginUser(credentials) {
  return fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

// const Login = ({ setToken }) => {
  const Login = () => {
  const navigate = useNavigate();
  const { setToken } = useContext(AuthContext);
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password,
    });
    setToken(token);
    if(token.token){ 
      navigate('/');
    }
  };

  return (
    <div className="login-wrapper">
      <h1>Please Login</h1>
      <form onSubmit={handleSubmit}>
        <label htmlFor="">
          <p>Username</p>
          <input type="text" onChange={(e) => setUserName(e.target.value)} />
        </label>

        <label htmlFor="">
          <p>Password</p>
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default Login;

// Login.propTypes = {
//   // setToken: PropTypes.func.isRequired,
// };

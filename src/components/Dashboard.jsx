import React from 'react'
import { useNavigate } from 'react-router-dom'


const Dashboard = () => {
    const navigate = useNavigate();
  return (
    <>
    <h2>Dashboard</h2>
    <button onClick={()=>navigate('/preference')} > To Preference</button>
    </>
    
  )
}

export default Dashboard
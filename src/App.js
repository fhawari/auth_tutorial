import Dashboard from './components/Dashboard';
import Preference from './components/Preference';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import Login from './components/Login';
import PrivateRoutes from './components/App/PrivateRoutes';
import { AuthContext, AuthProvider } from './components/App/AuthProvider';

function App() {
  return (
    <AuthProvider>
      <div className="wrapper">
        <h1 style={{ "textAlign": "center" }}>Application</h1>

        <AuthContext.Consumer>
          {({token,removeToken})=>(
            token && <button onClick={removeToken}> Log out</button>
          )}
        </AuthContext.Consumer>

        <Router>
          <Routes>
            <Route element={<PrivateRoutes />}>
              <Route path='/' element={<Dashboard />}></Route>
              <Route path='/dashboard' element={<Dashboard />}></Route>
              <Route path='/preference' element={<Preference />}></Route>
            </Route>
            <Route path='/login' element={<Login/>} />
            <Route path='*' element={<Navigate to='/' />} />
          </Routes>
        </Router>

      </div>
    </AuthProvider>
  );
}

export default App;
